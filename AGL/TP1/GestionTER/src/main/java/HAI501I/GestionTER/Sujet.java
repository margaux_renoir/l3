package HAI501I.GestionTER;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Sujet {
	private String id;
	private String titre;
	
	public Sujet() {
		this.id = null;
		this.titre = null;
	}
	
	public Sujet(String id, String titre) {
		this.id = id;
		this.titre = titre;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public String getTitre() {
		return this.titre;
	}
	
	public void serSujet () throws StreamWriteException, DatabindException, IOException {
		File fSujet = new File("target/sujet"+this.id+".json");
		ObjectMapper omSjt = new ObjectMapper();
		omSjt.writeValue(fSujet, this);
	}
	
	public void deserSujet(String nomfichier) throws StreamWriteException, DatabindException, IOException {
		ObjectMapper omSjt = new ObjectMapper();
		String path = "target/"+nomfichier;
		Sujet sjt = omSjt.readValue(new File(path), Sujet.class);
		this.id = sjt.id;
		this.titre = sjt.titre;
	}
	
	public String toString() {
		String l = this.titre + " (id : " + this.id + ")";
		return l;
	}
}
