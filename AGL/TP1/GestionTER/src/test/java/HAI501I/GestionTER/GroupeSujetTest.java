package HAI501I.GestionTER;

import junit.framework.TestCase;

public class GroupeSujetTest extends TestCase {

	//Attributs de la classe Sujet
	private String idS;
	private String titre;
	//Attributs de la classe Groupe
	private String idG;
	private String nom;
	private String idSujet;
	//Attributs de la classe Affectation
	private Groupe groupe;
	private Sujet sujet;
	
	Sujet sujet1 = new Sujet("s1", "titre1");
	Groupe groupe1 = new Groupe("g1", "nom1", "s1");
	
	Affectation a = new Affectation(Groupe groupe1, Sujet sujet1);
	
	//test Junit pour vérifier que : lorsqu’on affecte un sujet s à un groupe g, alors le sujet affecté à g est s et le groupe de s est g.
	@Test
	public void testAffSujetGroupe() {
		assertEquals("s1", sujet1.getIdSujet());
		assertEquals("g1", a.getGroupe());
	}
}
