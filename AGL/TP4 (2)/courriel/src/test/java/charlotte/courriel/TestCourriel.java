package charlotte.courriel;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
//import java.util.stream.Stream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class TestCourriel {
	ArrayList<String> pj1 = new ArrayList<String>();
	
	public void remplissagePJ1() {
		pj1.add("pj11");
		pj1.add("pj12");
	}
	
	ArrayList<String> mc = new ArrayList<String>();
	
	public void remplissageMC() {
		mc.add("bonjour");
		mc.add("merci");
		mc.add("cordialement");
	}
	
	ArrayList<String> pj2 = new ArrayList<String>();
	// Courriel fonctionnel
	Courriel courriel1 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", pj1);
	
	// Courriel mauvaise adresse 1
	Courriel courriel2 = new Courriel("0charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", pj1);
	
	// Courriel mauvaise adresse 2
	Courriel courriel3 = new Courriel("0charlotte.fabre.perso@gmail.", "Premier Courriel", "bonjour, comment vas-tu ?", pj1);
	
	// Courriel sans titre
	Courriel courriel4 = new Courriel("charlotte.fabre.perso@gmail.com", null, "bonjour, comment vas-tu ?", pj1);
		
	// Courriel impoli
	Courriel courriel5 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "comment vas-tu ?", pj1);
	
	// Courriel sans pj 1
	Courriel courriel6 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", pj2);

	// Courriel sans pj 2
	Courriel courriel7 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", null);
	
	
	@Test
	public void testMauvaiseAdresse() {
		assertEquals(courriel1.mauvaiseadresse(), false);
		assertTrue(courriel2.mauvaiseadresse());
		assertTrue(courriel3.mauvaiseadresse());
		assertEquals(courriel4.mauvaiseadresse(), false);
		assertEquals(courriel5.mauvaiseadresse(), false);
		assertEquals(courriel6.mauvaiseadresse(), false);
		assertEquals(courriel7.mauvaiseadresse(), false);
	}
	
	@Test
	public void testAbsenceMotCle() {
		remplissageMC();
		assertEquals(courriel1.absencemotcle(mc), false);
		assertEquals(courriel2.absencemotcle(mc), false);
		assertEquals(courriel3.absencemotcle(mc), false);
		assertEquals(courriel4.absencemotcle(mc), false);
		assertTrue(courriel5.absencemotcle(mc));
		assertEquals(courriel6.absencemotcle(mc), false);
		assertEquals(courriel7.absencemotcle(mc), false);
	}
	
	@Test
	public void testEnvoyer() {
		remplissagePJ1();
		assertTrue(courriel1.envoyer());
		assertEquals(courriel2.envoyer(), false);
		assertEquals(courriel3.envoyer(), false);
		assertEquals(courriel4.envoyer(), false);
		assertEquals(courriel5.envoyer(), false);
		assertEquals(courriel6.envoyer(), false);
		assertEquals(courriel7.envoyer(), false);
		
	}

	@ParameterizedTest
	@ValueSource(strings = {"adressemail", "123@gmail.com", "adresse@gmail", "adresse@.com", "@gmail.com"})
	public void testMauvaiseAdresseParam1(String adresse) {
		Courriel courrieltest = new Courriel(adresse, null, null, null);
		assertTrue(courrieltest.mauvaiseadresse());
	}
	
	
	public static Stream<Arguments> stringArrayProvider() {
	    return Stream.of(
	            Arguments.of(new String[]{"", "adresse vide"}),
	            Arguments.of(new String[]{"adressemail", "absence d'@"}),
	            Arguments.of(new String[]{"123@gmail.com", "début incorrect"})
	    );
	}
	
	@ParameterizedTest(name = "{1}")
	@MethodSource("stringArrayProvider")
	public void testMauvaiseAdresseParam2(String adresse, String message) {
		Courriel courrieltest = new Courriel(adresse, null, null, null);
		assertTrue(courrieltest.mauvaiseadresse());
	}
}
