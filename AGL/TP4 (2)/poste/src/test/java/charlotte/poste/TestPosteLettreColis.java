package charlotte.poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import charlotte.poste.src.main.java.poste.Colis;
import charlotte.poste.src.main.java.poste.Lettre;
import charlotte.poste.src.main.java.poste.Recommandation;

class TestPosteLettreColis {	
	// Instances pour tester
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.deux, true);
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	Colis colis2 = new Colis();
	private static float tolerancePrix=0.001f;
			
			
	// Test toString
	@Test
	public void testToString() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0", colis1.toString());
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire", lettre1.toString());
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence", lettre2.toString());
		assertEquals("Colis 0000/inconnue/0/0.0/0.0", colis2.toString());
	}

	// Test tarifAffranchissement
	@Test
	public void testTarifAffranchissement() {
		if ((lettre1.tarifAffranchissement()-1.0f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
		if ((lettre2.tarifAffranchissement()-2.3f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
		if ((colis1.tarifAffranchissement()-3.5f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
			
				
	// Test tarifRemboursement
	@Test
	public void testTarifRemboursement() {
		if ((lettre1.tarifRemboursement()-1.5f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
		if ((lettre2.tarifRemboursement()-15.0f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
		if ((colis1.tarifRemboursement()-100.0f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
}
