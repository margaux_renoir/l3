package charlotte.poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import charlotte.poste.src.main.java.poste.Colis;
import charlotte.poste.src.main.java.poste.ColisExpress;
import charlotte.poste.src.main.java.poste.ColisExpressInvalide;
import charlotte.poste.src.main.java.poste.ObjetPostal;
import charlotte.poste.src.main.java.poste.Recommandation;

class TestPosteColisExpress {
	// Instances à tester
	// Colis qui renvoie l'exception (car pas express, trop lourd)
	
	public TestPosteColisExpress() throws ColisExpressInvalide {}
	
	public void colis1() throws ColisExpressInvalide {
		ColisExpress colis1 = new ColisExpress("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200, true);
	}
	
			
	// Colis qui renvoie 33 comme tarif d'affranchissement 33
	ColisExpress colisexpress1 = new ColisExpress("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 29, 0.02f, Recommandation.deux, "train electrique", 200, true);

	// Colis qui renvoie  30 comme tarif d'affranchissement 30
	ColisExpress colisexpress2 = new ColisExpress("Maman",
			"famille Parienti, 9 avenue de la Méditerannée, Saint-Drézéry",
			"34160", 25, 1f, Recommandation.deux, "paquet de bonbons", 30, false);
	
	public ObjetPostal getObjet(ObjetPostal o) throws ColisExpressInvalide {
		return o;
	}
	
	// Test ColisExpressInvalide
	@Test
	public void testColisExpressInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> colis1());
	}
	
	// Test tarifAffranchissement
	@Test
	public void testTarifAffanchissement() {
		assertEquals(colisexpress1.tarifAffranchissement(), 33);
		assertEquals(colisexpress2.tarifAffranchissement(), 30);
	}
	
	// Test typeObjetPostal
	@Test
	public void testTypeObjetPostal() {
		assertEquals(colisexpress1.typeObjetPostal(), "Colis express");
		assertEquals(colisexpress2.typeObjetPostal(), "Colis express");
	}
	
	// Test toString()
	@Test
	public void testToString() {
		assertEquals(colisexpress1.toString(), "Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/29.0/2");
		assertEquals(colisexpress2.toString(), "Colis express 34160/famille Parienti, 9 avenue de la Méditerannée, Saint-Drézéry/2/1.0/30.0/25.0/3");
	}
	

}