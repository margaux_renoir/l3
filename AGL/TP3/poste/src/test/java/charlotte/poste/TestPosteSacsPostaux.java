package charlotte.poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import charlotte.poste.src.main.java.poste.Colis;
import charlotte.poste.src.main.java.poste.Lettre;
import charlotte.poste.src.main.java.poste.Recommandation;
import charlotte.poste.src.main.java.poste.SacPostal;

class TestPosteSacsPostaux {
	// Instances à tester
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.deux, true);
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	SacPostal sac1 = new SacPostal();
	public void remplissageSac() {
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
	}
	
	SacPostal sac2 = sac1.extraireV1("7877");
		
	// Test valeurRemboursement
	@Test
	public void testValeurRemboursement() {
		remplissageSac();
		if ((sac1.valeurRemboursement()-116.5f)<tolerancePrix) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
	
	public void testGetVolume() {
		remplissageSac();
		if ((sac1.getVolume()-0.025359999558422715f)<toleranceVolume) {
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
}
