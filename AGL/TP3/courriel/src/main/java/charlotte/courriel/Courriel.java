package charlotte.courriel;

import java.util.ArrayList;
import java.util.regex.Matcher;

public class Courriel {
	private String destination = "";
	private String titre = "";
	private String corps = "";
	private ArrayList<String> pj = new ArrayList<String>();
	
	public Courriel(String destination, String titre, String corps, ArrayList<String> pj) {
		this.destination = destination;
		this.titre = titre;
		this.corps = corps;
		this.pj = pj;
	}
	
	public String getDestination() {
		return this.destination;
	}
	
	public void setDestination(String d) {
		this.destination = d;
	}
	
	public String getTitre() {
		return this.titre;
	}
	
	public void setTitre(String t) {
		this.titre = t;
	}
	
	public String getCorps() {
		return this.corps;
	}
	
	public void setCorps(String c) {
		this.corps = c;
	}
	
	public ArrayList<String> getPJ() {
		return this.pj;
	}
	
	public void setTitre(ArrayList<String> pj) {
		this.pj = pj;
	}
	
	public boolean mauvaiseadresse() {
		return (!(this.destination.matches("[a-z].*[@][a-z]+[.][a-z]+")));
	}
	
	public boolean absencemotcle(ArrayList<String> mc) {
		boolean b = true;
		for (int i = 0; i < mc.size(); i++) {
			String m = mc.get(i);
			if (this.corps.matches(".*"+m+".*")) {
				b = false;
			}
		}
		return b;
	}
	
	public boolean envoyer () {
		ArrayList<String> mc = new ArrayList<String>();
		mc.add("bonjour");
		mc.add("cordialement");
		mc.add("merci");
		
		if (this.mauvaiseadresse()) {
			return false;
		}
		if (this.titre == "" || this.titre == null) {
			return false;
		}
		if (this.corps == "" || this.corps == null || this.absencemotcle(mc)) {
			return false;
		}
		if(this.pj == null || this.pj.size() <= 0) {
			return false;
		}
		return true;
	}
	
	
}
