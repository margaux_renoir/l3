package charlotte.courriel;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class TestCourriel {
	ArrayList<String> pj1 = new ArrayList<String>();
	
	public void remplissagePJ1() {
		pj1.add("pj11");
		pj1.add("pj12");
	}
	
	ArrayList<String> pj2 = new ArrayList<String>();
	// Courriel fonctionnel
	Courriel courriel1 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", pj1);
	
	// Courriel mauvaise adresse 1
	Courriel courriel2 = new Courriel("0charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", pj1);
	
	// Courriel mauvaise adresse 2
	Courriel courriel3 = new Courriel("0charlotte.fabre.perso@gmail.", "Premier Courriel", "bonjour, comment vas-tu ?", pj1);
	
	// Courriel sans titre
	Courriel courriel4 = new Courriel("charlotte.fabre.perso@gmail.com", null, "bonjour, comment vas-tu ?", pj1);
		
	// Courriel impoli
	Courriel courriel5 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "comment vas-tu ?", pj1);
	
	// Courriel sans pj 1
	Courriel courriel6 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", pj2);

	// Courriel sans pj 2
	Courriel courriel7 = new Courriel("charlotte.fabre.perso@gmail.com", "Premier Courriel", "bonjour, comment vas-tu ?", null);
	
	
	@Test
	public void testEnvoyer1() {
		remplissagePJ1();
		assertTrue(courriel1.envoyer());
	}
	
	@Test
	public void testEnvoyer2() {
		remplissagePJ1();
		assertEquals(courriel2.envoyer(), false);
	}
	
	@Test
	public void testEnvoyer3() {
		remplissagePJ1();
		assertEquals(courriel3.envoyer(), false);
	}
	
	@Test
	public void testEnvoyer4() {
		remplissagePJ1();
		assertEquals(courriel4.envoyer(), false);
	}
	
	@Test
	public void testEnvoyer5() {
		remplissagePJ1();
		assertEquals(courriel5.envoyer(), false);
	}
	
	@Test
	public void testEnvoyer6() {
		assertEquals(courriel6.envoyer(), false);
	}
	
	@Test
	public void testEnvoyer7() {
		assertEquals(courriel7.envoyer(), false);
	}
}
