#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 10:43:32 2021

@author: margaux.renoir@etu.umontpellier.fr
"""

from random import * # chargement de toute la bibliothèque
from matplotlib import pyplot as plt # chargement d'un des modules de matplotlib

L = []
L2 = []
L3 = []
L4 = []

def entiersAleatoires(n,a,b):
    for i in range(n):
        L.append(randint(a,b))
    return L

def entiersAleatoires2(n,a,b):
    for i in range(n):
        L2.append(randrange(a,b))
    return L2

Li = entiersAleatoires(1000,1,100)
#print(Li)

Li2 = entiersAleatoires2(1000,1,100)
#print(Li2)

plt.hist(Li, bins=100) # histogramme avec 100 colonnes
#plt.show() # Affichage du résultat

plt.hist(Li2, bins=100) # histogramme avec 100 colonnes
#plt.show() # Affichage du résultat

def flottantsAleatoires(n):
    for i in range(n):
        L3.append(random())
    return L3

def flottantsAleatoires2(n,a,b):
    for i in range(n):
        L4.append(uniform(a,b))
    return L4

    
Li3 = flottantsAleatoires(1000)
#print(Li3)

Li4 = flottantsAleatoires2(1000,-3,10)
#print(Li4)

plt.plot(Li3) # Ligne brisée
#plt.show()

plt.plot(Li4) # Ligne brisée
#plt.show()


Liste = []
Liste2 = []

def pointsDisque(n):
    while n > 0:
        x = uniform(-1,1)
        #print("x = ",x)     
        y = uniform(-1,1)
        #print("y = ",y)
        if (x**2 +y**2) <= 1:
            Liste.append(x)
            Liste.append(y)
            n = n-1
    return Liste

def pointsDisque2(n):
    while n > 0:
        x = uniform(-1,1)
        #print("x = ",x)     
        y = uniform(-1,1)
        #print("y = ",y)
        if (x**2 +y**2) <= 1:
            Liste2.append(x)
            Liste2.append(y)
            n = n-1
        else :
            y = uniform(-1,1)
            #print("x2 = ",x)
            #print("y2 = ",y)         
            if (x**2 +y**2) <= 1:
                Liste2.append(x)
                Liste2.append(y)
                n = n-1           
    return Liste2
    
ListePoint = pointsDisque(10)
print("ListePoint = ",ListePoint)

ListePoint2 = pointsDisque2(10)
















