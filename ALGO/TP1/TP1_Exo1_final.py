#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 10:43:32 2021

@author: margaux.renoir@etu.umontpellier.fr
"""

from random import * # chargement de toute la bibliothèque
from matplotlib import pyplot as plt # chargement d'un des modules de matplotlib

L = []
L2 = []
L3 = []
L4 = []

#def entiersAleatoires(n,a,b):
 #   for i in range(n):
  #      L.append(randint(a,b))
   # return L

#def entiersAleatoires2(n,a,b):
 #   for i in range(n):
  #      L2.append(randrange(a,b))
   # return L2


#Li = entiersAleatoires(1000,1,100)
#print(Li)
 
#Li2 = entiersAleatoires2(1000,1,100)
#print(Li2)
 
#plt.hist(Li, bins=100) # histogramme avec 100 colonnes
#plt.show() # Affichage du résultat

#plt.hist(Li2, bins=100) # histogramme avec 100 colonnes
#plt.show() # Affichage du résultat


#def flottantsAleatoires(n):
 #   for i in range(n):
  #      L3.append(random())
   # return L3

#def flottantsAleatoires2(n,a,b):
 #   for i in range(n):
  #      L4.append(uniform(a,b))
   # return L4

    

#Li3 = flottantsAleatoires(1000)
#print(Li3)
 
#Li4 = flottantsAleatoires2(1000,-3,10)
#print(Li4)
 
#plt.plot(Li3) # Ligne brisée
#plt.show()
 
#plt.plot(Li4) # Ligne brisée
# plt.show()
 


Liste = []
Liste2 = []

def pointsDisque(n):
    while n > 0:
        x = uniform(-1,1)
        #print("x = ",x)     
        y = uniform(-1,1)
        #print("y = ",y)
        if (x**2 +y**2) <= 1:
            Liste.append(x)
            Liste.append(y)
            n = n-1
    return Liste

def pointsDisque2(n):
    while n > 0:
        x = uniform(-1,1)
        #print("x = ",x)     
        y = uniform(-1,1)
        #print("y = ",y)
        if (x**2 +y**2) <= 1:
            Liste2.append(x)
            Liste2.append(y)
            n = n-1
        else :
            y = uniform(-1,1)
            #print("x2 = ",x)
            #print("y2 = ",y)         
            if (x**2 +y**2) <= 1:
                Liste2.append(x)
                Liste2.append(y)
                n = n-1           
    return Liste2
    
#ListePoint = pointsDisque(10)
#print("ListePoint = ",ListePoint)

#ListePoint2 = pointsDisque2(10)


def affichagePoints(ListeA):
    X = [x for x,y in ListeA]
    Y = [y for x,y in ListeA]
    plt.scatter(X, Y, s = 1)
    plt.axis('square')
    plt.show()
     
#affichagePoints(pointsDisque(10))
  

def aleatoireModulo(N):
    k = 8
    x = getrandbits(k)
    #print(x)
    return (x%N)

mod = aleatoireModulo(5)
#print(mod)

def aleatoireRejet(N):
    k = 8
    x = getrandbits(k)
    #print(x)

    while x >= N:
        x = getrandbits(k)
        #print(x)    
    return x
    
mod2 = aleatoireRejet(5)
print(mod2)

#Générer deux suites de 10 000 entiers aléatoires entre 0 et 100 avec aleatoireModulo et aleatoireRejet.
#Représenter graphiquement chacune des deux suites sous forme d’histogramme. Laquelle des deux
#méthodes produit des entiers uniformes entre 0 et N −1 ?

suite = []
def suitealeatoireModulo(N):
    for i in range(N):
        suite.append(aleatoireModulo(100))
    return suite

suite1 = suitealeatoireModulo(10000)
#print(suite1)

suite = []
def suitealeatoireRejet(N):
    for i in range(N):
        suite.append(aleatoireRejet(100))
    return suite

suite2 = suitealeatoireRejet(10000)
#print(suite2)

plt.hist(suite1, bins=100) # histogramme avec 100 colonnes
#plt.show()

plt.hist(suite2, bins=100) # histogramme avec 100 colonnes
#plt.show()






















