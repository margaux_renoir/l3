#! /bin/env python3

from random import * # chargement de toute la bibliothèque
from matplotlib import pyplot as plt # chargement d'un des modules de matplotlib

def suivant(xn, a, c, m):
   return (a*xn+c) % m


def valeurs(x0, a, c, m, N):

    xn=[]
    xn.append(x0)
    for i in range(1,N):

        xn.append((a*xn[i-1]+c) % m)

        
    return xn


#print(valeurs(0,3,5,136,20))



#la suite est cyclique


tab=valeurs(1000,75,74,(2**16)+1,2**20)

def rechercheRepet(tab):
    taille=len(tab)
    i=0
    monint=-1
    for j in range(i+1,taille):
            if(tab[i]==tab[j]):
                i=i+1
                if( monint==-1):
                    monint=j
            else: monint=-1    
            if(i==monint):
                return monint
    return False


#print(rechercheRepet(valeurs(1000,75,74,(2**16)+1,2**20)))

#print(tab[0])
#print(tab[65536])
#print(tab[1])
#print(tab[65537])
#print(tab[2])
#print(tab[65538])
#print(tab[3])
#print(tab[65539])


tab0=valeurs(1000,16807,0,(2**31)-1,2**20)

plt.plot(tab, 'r.')
#plt.show()


tab1=valeurs(123489,168807,0,(2**31)-1,10000)

tab2=valeurs(25743,168807,0,(2**31)-1,10000)

plt.scatter(tab1,tab2,s=.2)
#plt.show()



tab3=valeurs(123488,168807,0,(2**31)-1,2**20)

tab4=valeurs(123489,168807,0,(2**31)-1,2**20)

plt.scatter(tab3,tab4, s=.2)
#plt.show()


tab5=valeurs(5,168807,0,(2**31)-1,2**20)

tab6=valeurs(6,168807,0,(2**31)-1,2**20)

plt.scatter(tab5,tab6, s=.2)
#plt.show()



class Generateur:

   __x=None
   __a=0
   __b=0
   __m=0
   
   def __init__(self,a,b,m):
      self.__a=a
      self.__b=b
      self.__m=m

   def graine(self,g):

       self.__x=g

   def aleatoire(self):
      if(self.__x==None):
          print("erreur")
      else:
          self.__x=(self.__a*self.__x+self.__b)%self.__m
          print(self.__x)
   def __str__(self):
      return print("x= "+str(self.__x) +" a= "+str(self.__a) +" b= "+str(self.__b) +" m= "+str(self.__m))
   def __repr__(self):
      return  __str__()
   
g = Generateur(3, 5, 17)
g.aleatoire()
g.graine(5)
g.aleatoire()
g.__repr__()
