#! /bin/env python3

from random import * # chargement de toute la bibliothèque
from matplotlib import pyplot as plt # chargement d'un des modules de matplotlib


def entiersAleatoires(n,a,b) :
    
    tableau = []
    
    for i in range(n) :

        tableau.append(randint(a,b))

    return tableau


def eltMajDet(T) :

    for i in range(len(T)):
        c=0
        for j in range(len(T)):
            if (T[i]==T[j]):
                c=c+1
        if(c>(len(T)/2)):
            return  T[i]

    

T=[2,3,3,3,2]
           


def eltMajProba(T) :
    c=0

    while(c != len(T)/2):
        memoire=choice(T)
        for i in range(len(T)):
            if(T[i]==memoire):
                c=c+1
        if (c>(len(T)/2)):
                return memoire
    
T=[2,3,3,3,2]
           
#print(eltMajDet(T))
#print(eltMajProba(T))


def tabAlea(n, a, b, k):

    tab= []
    m=entiersAleatoires(1,a,b)[0]
    cpt=n-k
    for i in range(k):
        tab.append(m)
    
        while(cpt>0):
            test=entiersAleatoires(1,a,b)[0]
            if(test !=m):
                tab.append(test)
                cpt=cpt-1
    shuffle(tab)    
    return tab



print(tabAlea(10, 0,10, 6))

                   
def tabDeb(n,a,b,k):

    tab= []
    m=entiersAleatoires(1,a,b)[0]
    cpt=n-k
    for i in range(k):
        tab.append(m)
    
        while(cpt>0):
            test=entiersAleatoires(1,a,b)[0]
            if(test !=m):
                tab.append(test)
                cpt=cpt-1
    shuffle(tab)
    if(tab[0]==m):
        return tab
    else :
        tampon=tab[0]
        tab[0]=m
        i=1
        while (T[i] != T[0]):
            i=i+1
        T[i]=tampon
    return tab


print(tabDeb(10, 0,10, 6))


def tabFin(n, a,b,k):

    tab= []
    t= []
    m=entiersAleatoires(1,a,b)[0]
    cpt=n-k
    
    while(cpt>0):
        test=entiersAleatoires(1,a,b)[0]
        if(test !=m):
            tab.append(test)
            cpt=cpt-1
    shuffle(tab)
    for j in range(n-k):
        t.append(tab[j])
    
    for i in range(k):
        t.append(m)
    return t
print(tabFin(10,0,10,6))



def contientEltMaj(T, m):
    cpt=0
    for i in range(m):
        e=choice(T)
        for j in range(len(T)):
            if(T[j]==e):
                cpt=cpt+1
                if (cpt>(len(T)/2)):
                    return True
        
          
    return False


tabx=[1,2,2,1,1,1,5,5,2,2,2,8,8]

print(contientEltMaj(tabx,2))


def testContient(n, a, b, k, m, N):

    cpt=0
    for i in range(N):
       tableau=tabAlea(n, a, b, k)
    if(contientEltMaj(tableau, m)):
        cpt=cpt+1

    return cpt/N

print(testContient(1000,0,10,500,10,1000))

