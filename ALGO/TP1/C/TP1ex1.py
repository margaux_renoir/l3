#! /bin/env python3

from random import * # chargement de toute la bibliothèque
from matplotlib import pyplot as plt # chargement d'un des modules de matplotlib



def entiersAleatoires(n,a,b) :
    
    tableau = []
    
    for i in range(n) :

        tableau.append(randint(a,b))

    return tableau



L1=entiersAleatoires(100000,1,100)

def entiersAleatoires2(n,a,b):

    tableau = []

    for i in range(n) :

        tableau.append(randrange(a,b))

    return tableau

L2=entiersAleatoires2(100000,1,100)


#plt.hist(L1, bins=100) # histogramme avec 100 colonnes
#plt.show() 

#plt.hist(L2, bins=100) # histogramme avec 100 colonnes
#plt.show() 

#randint semble a peu près homogène, alors que randrange a un trou



def flottantsAleatoires(n) :

    tableau = []

    for i in range(n):

        tableau.append(random())
    
    return tableau

L3=flottantsAleatoires(100000)

#plt.hist(L3, bins=100) # histogramme avec 100 colonnes
#plt.show() 

#plt.plot(L3) # Ligne brisée
#plt.show()
def flottantsAleatoires2(n,a,b) :

    tableau =[]

    for i in range(n) :

        tableau.append(uniform(a,b))

    return tableau


L4=flottantsAleatoires2(100000,-3,10)

#plt.hist(L4, bins=100) # histogramme avec 100 colonnes
#plt.show() 

#plt.plot(L4) # Ligne brisée
#plt.show()


def  pointsDisque(n) :

    tableau= []
    listeDeMort= []
    cpt=n

    while(cpt>0) :
        
        x=2*random()-1
        y=2*random()-1

        if((x*x+y*y)<=1):
            listeDeMort= []
            listeDeMort.append(x)
            listeDeMort.append(y)
            tableau.append(listeDeMort)
            cpt=cpt-1
    
    return tableau

print(pointsDisque(2))


def pointsDisque2(n) :

    tableau= []
    cpt=n
    x=2*random()-1
    while(cpt>0) :
       
        y=2*random()-1

        if((x*x+y*y)<=1):
            listeDeMort= []
            listeDeMort.append(x)
            listeDeMort.append(y)
            tableau.append(listeDeMort)
            cpt=cpt-1
            x=2*random()-1
    
    return tableau
        
print(pointsDisque2(10))



def affichagePoints(L):
    X = [x for x,y in L] # on récupère les abscisses...
    Y = [y for x,y in L] # ... et les ordonnées
    plt.scatter(X, Y, s = 1) # taille des points minimale
    plt.axis('square') # même échelle en abscisse et ordonnée
    plt.show()


#affichagePoints(pointsDisque(10000))
#affichagePoints(pointsDisque2(10000))

LD=pointsDisque(10000)
LD2=pointsDisque(10000)

#affichagePoints(LD)
#affichagePoints(LD2)


def aleatoireModulo(N):

   return getrandbits(N.bit_length()) % N

print(aleatoireModulo(14))

def  aleatoireRejet(N):
    
    while(getrandbits(N.bit_length()) >= N):
        getrandbits(N.bit_length())
    return getrandbits(N.bit_length())

print(aleatoireRejet(14))


liste1=[]

liste2= []

for i in range(10000):
    liste1.append(aleatoireModulo(100))
    liste2.append(aleatoireRejet(100))

#plt.hist(liste1, bins=100) # histogramme avec 100 colonnes
#plt.show()

#plt.hist(liste2, bins=100) # histogramme avec 100 colonnes
#plt.show() 

