#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 24 14:14:39 2021

@author: margaux.renoir@etu.umontpellier.fr
"""

def clause(s):
    clauses = []
    s2 = s.split()
    
    for i in range(0,len(s2)-1):
        clauses.append(int(s2[i]))
    print("clause = ",clauses)
    return clauses
    
#clause("1 2 -3 0 \n")

def parseur(nom):
    F = []
    tupleF = (2)
    nbrV = 0
    nbrC = 0
    with open(nom) as f:
        for ligne in f:
            if ligne[0] != 'c':
                if ligne[0] == 'p':
                    print("ligne =",ligne)
                    ligneS = ligne.split()
                    print("ligneS =",ligneS)
                    nbrV = int(ligneS[2])
                    print('nbrV = ',nbrV)
                    nbrC = int(ligneS[3])
                    print('nbrC = ',nbrC)
                else:
                    F.append(clause(ligne))
    tupleF = (F,nbrV)
    return tupleF
        
#FF = parseur('/home/e20160002664/L3/ALGO/TP2/cnf/simple_v3_c2.cnf')
#print("FF = ",FF)    

def est_valide(F,A):
    for clause in F:
        ok = False
        for lit in clause:
            if (lit * A[abs(lit)-1]) > 0:
                ok = True
        if not ok:
            return False
    return True

F1 = parseur('/home/e20160002664/L3/ALGO/TP2/cnf/simple_v3_c2.cnf')[0]
n = parseur('/home/e20160002664/L3/ALGO/TP2/cnf/simple_v3_c2.cnf')[1]

#r = est_valide(F1,[1,1,-1])
#print('r = ',r)
#r = est_valide(F1,[-1,-1,1])
#print('r = ',r)

def aff_suivante(A):
    i = 1
    n = len(A)
    while i < n and A[i] == 1:
        A[i] = -1
        i += 1 
    if i == n:
        return None
    A[i] = 1
    
    return A
        
#r = aff_suivante([1,-1,-1,-1])   
#print(r)
    
def sat_exhau(F, n):
        A = [ -1 for i in range(n)]
        
        while not est_valide(F,A):
            A = aff_suivante(A)
            
            if A == None:
                return "Insatisfiable"
        return A

#At = sat_exhau(F1,n)    
#print('At = ',At)
        
def elimination(F, n, b):
    Fvide = []
    
    for clause in F:
        cVide = []
        SAT = False
        
        for lit in clause:
            if abs(lit) == n and (lit*b) > 0:
                SAT = True
            elif abs(lit) != n:
                cVide.append(lit)
        if not SAT:
            Fvide.append(cVide)
            
    return Fvide
    
#r = elimination(F1,n,-1)
#print("r =",r)
                
#Écrire une fonction sat_backtrack(F, n) qui prend en entrée une formule F à n variables et
#renvoie une affectation A (de longueur n) qui satisfait F s’il en existe une, None sinon, par backtrack.
#Tester avec la formule du fichier simple_v2_c2.cnf, puis avec celles des fichiers random-...cnf
#pour différentes valeurs de i. Jusqu’à quelle valeur de i pouvez-vous trouver une solution pour une formule
#satisfiable en environ 1 seconde ? Et pour les formules insatisfiables ? La formule contenue dans le fichier
#hole6.cnf est-elle satisfiable ?


def sat_backtrack(F, n):
    if F == []:
        A = [ 1 for i in range(n)]
        return A
    if [] in F:
        return None
    for b in (1,-1):
        Fvide = elimination(F,n,b)
        A = sat_backtrack(Fvide, n-1)
        if A is not None:
            return A + [b]
    return None

F2 = parseur('/home/e20160002664/L3/ALGO/TP2/cnf/random-50-sat.cnf')[0]
n2 = parseur('/home/e20160002664/L3/ALGO/TP2/cnf/random-50-sat.cnf')[1]
#print("n2 = ",n2)

r = sat_backtrack(F2, n2)
print("r = ",r)









        
        
        