package fr.umfds.agl;

public class ColisExpressInvalide extends Exception {

	public ColisExpressInvalide(String string) {
		super(string);
	}

}
