package fr.umfds.agl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;

public class TestColis {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	// création des objets sous test
		Lettre lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		Lettre lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true); 
		Colis colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		
	// tests sur les sacs postaux
		SacPostal sac1 = new SacPostal();
		@Test
		public void remplissageSac() {
			
			sac1.ajoute(lettre1);
			sac1.ajoute(lettre2);
			sac1.ajoute(colis1);
			
			assertEquals(116.5f, sac1.valeurRemboursement(), tolerancePrix);
		}
		
		public void getVolume() {
			assertEquals(0.025359999558422715f, sac1.getVolume(), toleranceVolume);
		}
		SacPostal sac2 = sac1.extraireV1("7877");
		
		public void getVolume2() {
			assertEquals(0.02517999955569394f, sac2.getVolume(), toleranceVolume);
		}
}



