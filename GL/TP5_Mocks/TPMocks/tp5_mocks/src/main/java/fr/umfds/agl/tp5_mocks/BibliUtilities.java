package fr.umfds.agl.tp5_mocks;

import java.util.ArrayList;
/*

chercherNoticesConnexes(NoticeBibliographique ref)
    retourne des suggestions de lecture "connexes" à ref
    pour l'instant il a été décidé que les notices connexes seraient constituées de 5 notices bibliographiques du même auteur que ref,
    mais de titre différent (différent du titre de ref et de plus les 5 notices doivent toutes être de titre différent).
ajoutNotice(String isbn)
    ajoute la notice bibliographique dont l'isbn est reçu en paramètre à la liste des notices connues de la bibliothèque.
    si l'isbn ne correspond à aucune notice bibliographique cette méthode jette une AjoutImpossibleException.
    cette méthode renvoie le statut de la notice ajoutée, parmi : newlyAdded (la notice n'existait pas), updated (la notice existait et a été modifiée), nochange (la notice existait en l'état), notExisting (la notice est null).
prévoirInventaire()
    Détermine s'il faut prévoir un inventaire
    On prévoit un inventaire si le dernier inventaire était il y a 12 mois ou plus.
    On veillera pour celà à ajouter un attribut représentant l'horloge (Clock) utilisée dans BibliUtilities, puis on accèdera à la date courante via LocalDate.now(horloge);
*/

public class BibliUtilities {
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref) {
		
		
		return null;
	}


}
