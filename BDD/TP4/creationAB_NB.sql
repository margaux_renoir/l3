/*Créer une table AB_NB qui doit comporter seulement deux attributs : NUMERO dont la
définition doit être compatible avec NUM_AB de ABONNE et NB défini comme un
entier sur 3 positions*/

/*
Suppression des relations 
*/
/*
*************************************************
ATTENTION NE PAS TOUCHER AUX LIGNES SUIVANTES
ELLES PERMETTENT DE SUPPRIMER PROPREMENT LES RELATIONS
*************************************************
*/
prompt "Suppression des relations"
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE AB_NB';
EXCEPTION
 WHEN OTHERS THEN
	IF SQLCODE != -942 THEN
	RAISE;
	END IF;
END;
/


/*
*************************************************
*/


/*
Création des relations 
*/

prompt "Création des relations"

CREATE TABLE AB_NB (
	NUMERO NUMERIC(6,0),
	NB NUMERIC(3,0),
	CONSTRAINT PK_AB_NB PRIMARY KEY (NUMERO)
	);
