/*A l'aide de votre éditeur préféré, définissez le bloc PL/SQL permettant : 
De stocker dans une variable le nombre d'emprunt d'un abonné ;
D’insérer le couple formé du numéro d'abonné et du nombre d'emprunts de cet abonné dans la table de travail AB_NB ;*/
prompt "debut";

DECLARE
    nombre_emprunt_abonne NUMBER(3,0);
    num_abonne NUMBER(6,0);
    num_abonne2 ABONNE.NUM_AB%type;
    pasEmprunt exception;
    prompt "milieu";
BEGIN 
    num_abonne := &num_abo;
    SELECT NUM_AB INTO num_abonne2 FROM ABONNE WHERE NUM_AB = num_abonne;
    SELECT COUNT(*) INTO nombre_emprunt_abonne FROM EMPRUNT WHERE NUM_AB = num_abonne AND d_retour IS NOT NULL; 
    IF nombre_emprunt_abonne = 0 THEN raise pasEmprunt; end IF;
    INSERT INTO AB_NB VALUES (num_abonne,nombre_emprunt_abonne);
    exception
    WHEN no_data_found THEN INSERT INTO AB_NB VALUES (numero,NULL); 
    WHEN pasEmprunt THEN INSERT INTO AB_NB VALUES (numero,-1); 


END;
/ -- exécuCon du bloc PL/SQL

--SELECT NUMERO FROM AB_NB;




    