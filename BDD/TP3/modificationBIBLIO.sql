--Q4
ALTER TABLE ABONNE ADD DATE_NAI DATE;
ALTER TABLE ABONNE ADD TYPE_AB VARCHAR(20) CONSTRAINT DOM_TYPE_AB CHECK (TYPE_AB IN ('ADULTE','ENFANT'));
ALTER TABLE ABONNE ADD CAT_AB VARCHAR(10) CONSTRAINT DOM_CAT_AB CHECK (CAT_AB IN ('REGULIER', 'OCCASIONNEL', 'A PROBLEME', 'EXCLU'));

DESC ABONNE;

INSERT INTO ABONNE VALUES (901001,'LEVEQUE','PIERRE','MONTPELLIER',40,500,NULL,'ADULTE','REGULIER','10-04-2021') ;
